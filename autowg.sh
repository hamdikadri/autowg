#!/bin/bash
#
# AUTOWG written by Hamdi KADRI 
# APACHE LICENSE version 2.0 applies
# This script is intended to create configurations for 
# a point-to-point Wireguard connection between a server
# and a client (/30 network)
#

# Step zero: declare configurations as variables

servercfg="[Interface]
Address = <serverwgIP>
SaveConfig = true
ListenPort = <port>
PrivateKey = <server-privatekey>
[Peer]
PublicKey = <client-pubkey>
AllowedIPs = <clientwgIP>"

clientcfg="[Interface]
PrivateKey = <client-privatekey>
Address = <clientwgIP>
[Peer]
PublicKey = <server-pubkey>
AllowedIPs = <clientwgIP>
EndPoint = <serverIP>:<port>
PersistentKeepalive = 20"

postcfg="[Interface]
Address = <serverwgIP>
SaveConfig = true
ListenPort = <port>
PrivateKey = <server-privatekey>
PostUp = iptables -A FORWARD -i <wgintname> -j ACCEPT
PostUp = iptables -t nat -A POSTROUTING -o <srvinternetintname> -j MASQUERADE
PostDown = iptables -D FORWARD -i <wgintname> -j ACCEPT
PostDown = iptables -t nat -D POSTROUTING -o <srvinternetintname> -j MASQUERADE
[Peer]
PublicKey = <client-pubkey>
AllowedIPs = <clientwgIP>
"

# Step one: ask for some parameters (as an assistant)
# We need: point-to-point IPs, Server IP, port

echo "AutoWG requires some informations before generating your config"
echo "Please provide the next parameters."
echo "This script will not check if the IPs and netmask are valid!"
echo "Press Enter to continue.."
echo
read
read -p "Server IP for the Wireguard interface: " serverwgIP
read -p "Client IP for the Wireguard interface: " clientwgIP
read -p "Network Mask (in CIDR) for both server and client WG interfaces (example: /30): " netmask
read -p "Server Public IP address: " serverIP 
read -p "Network Port for Wireguard communication: " port
read -p "Wireguard interface name? (for example wg0): " wgintname
read -p "Route all traffic to server via Wireguard? [y/N]: " internetaccess
if [[ "$internetaccess" =~ ^([yY][eE][sS]|[yY])$ ]]
then
	clientcfg=$(echo "$clientcfg" | sed "s|AllowedIPs = <clientwgIP>|AllowedIPs = 0.0.0.0/0|g" )
	read -p "Which server interface has internet access? " srvinternetintname
    servercfg=$(echo "$postcfg" | sed "s|<wgintname>|${wgintname}|g" | sed "s|<srvinternetintname>|${srvinternetintname}|g" )
	echo
    RED='\033[0;31m'
    NC='\033[0m' # No Color
    printf "${RED}IMPORTANT:${NC} You need to enable IP Forwarding on the server\n"
    echo "On Linux servers, uncomment the line \"net.ipv4.ip_forward=1\" in /etc/sysctl.conf"
    echo "then run \"sysctl -p\""
fi
# Step two: generate keypairs
## Generate keypairs for machine 1 (client)
client_prvkey=$(wg genkey)
client_pubkey=$(echo $client_prvkey | wg pubkey)

## Generate keypairs for machine 2 (server)
server_prvkey=$(wg genkey)
server_pubkey=$(echo $server_prvkey | wg pubkey)

# Step three: generate configuration

serverconf=$(echo "$servercfg" | sed "s|<serverwgIP>|${serverwgIP}${netmask}|g" | \
	sed "s|<port>|${port}|g" | sed "s|<server-privatekey>|${server_prvkey}|g" |\
	sed "s|<client-pubkey>|${client_pubkey}|g" | sed "s|<clientwgIP>|${clientwgIP}|g" )

clientconf=$(echo "$clientcfg" | sed "s|<client-privatekey>|${client_prvkey}|g" | \
	sed "s|<clientwgIP>|${clientwgIP}${netmask}|g" | sed "s|<server-pubkey>|${server_pubkey}|g" | \
	sed "s|<serverIP>|${serverIP}|g" | sed "s|<port>|${port}|g" )

# Step four: display configuration for machine 1 (client)
echo 
echo "** Client Side /etc/wireguard/${wgintname}.conf **"
echo "$clientconf"
echo

# Step five: display configuration for machine 2 (server)
echo 
echo "** Server Side /etc/wireguard/${wgintname}.conf **"
echo "$serverconf"
echo

# Step Seven: Saving to a text file 
#
echo "** Client Side /etc/wireguard/${wgintname}.conf **" > wireguard-conf.txt
echo "$clientconf" >> wireguard-conf.txt
echo  >> wireguard-conf.txt
echo "** Server Side /etc/wireguard/${wgintname}.conf **" >> wireguard-conf.txt
echo "$serverconf" >> wireguard-conf.txt
echo >> wireguard-conf.txt

